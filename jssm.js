///////////////////////////////////////
// MODULE GLOBALS
///////////////////////////////////////

const machineTypes = {
    ASM: Symbol("ASM"),
    FSM: Symbol("FSM"),
    PSM: Symbol("PSM")
}

const registeredMachines = {};
const machineInstances = {};
let nextMachineId = 0;

///////////////////////////////////////
// FUNCTIONS
///////////////////////////////////////

function registerASM(name, proto = {})
{
    registeredMachines[name] = {
        type: machineTypes.ASM,
        onEnter: proto.onEnter || null,
        onExit: proto.onExit || null,
        onStateChange: proto.onStateChange || null,
    };
}

function registerFSM(name, proto = {})
{
    registeredMachines[name] = {
        type: machineTypes.FSM,
        initialState: proto.initialState || null,
        states: proto.states || {},
        onEnter: proto.onEnter || null,
        onExit: proto.onExit || null,
        onStateChange: proto.onStateChange || null,
    };
}

function registerPSM(name, proto = {})
{
    registeredMachines[name] = {
        type: machineTypes.PSM,
        initialStates: proto.initialStates || [],
        states: proto.states || {},
        onEnter: proto.onEntry || null,
        onExit: proto.onExit || null,
        onStateChange: proto.onStateChange || null,
    };
}

///////////////////////////////////////

function createMachine(name, data = {})
{
    let id = Symbol(name + ":" + (nextMachineId++));
    let proto = registeredMachines[name];
    let instance = createInstanceData(proto, data);
    populateInstanceDataWithStates(instance, proto, data);
    machineInstances[id] = instance;
    return id;
}

function createInstanceData(proto, data)
{
    return {
        type: proto.type,
        initialState: data.initialState || proto.initialState,
        currentState: data.currentState || proto.initialState,
        initialStates: data.initialStates || proto.initialStates,
        currentStates: data.currentStates || proto.initialStates,
        onEnter: data.onEnter || proto.onEnter,
        onExit: data.onExit || proto.onExit,
        onStateChange: data.onStateChange || proto.onStateChange,
    }
}

function populateInstanceDataWithStates(instance, proto, data)
{
    instance.states = {};

    for (let stateName in proto.states)
    {
        if (data.states && data.states[stateName] === "symbol")
            instance.states[stateName] = data.states[stateName];
        else
            instance.states[stateName] = createMachine(proto.states[stateName], (data.states || {})[stateName] || {})
    }
}

///////////////////////////////////////
// EXPORTS
///////////////////////////////////////

let debug = {
    registeredMachines,
    machineInstances,
}

export default {
    registerASM,
    registerFSM,
    registerPSM,
    createMachine,
    debug
};
