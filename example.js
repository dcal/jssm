import jssm from "./jssm.js";

///////////////////////////////////////

jssm.registerPSM("Exam", {
    initialStates: ["questions", "pausable"],
    states: {
        questions: "Questions",
        pausable: "Pausable"
    }
});

// 'Questions' will be dynamic and therefore doesn't have any prototype data
jssm.registerFSM("Questions");

jssm.registerPSM("Question", {
    initialStates: ["radio1", "radio2", "radio3"],
    states: {
        radio1: "Radio",
        radio2: "Radio",
        radio3: "Radio",
    }
})

jssm.registerFSM("Radio", {
    initialState: "empty",
    states: {
        empty: "RadioButtonEmpty",
        full: "RadioButtonFull"
    }
})

jssm.registerASM("RadioButtonEmpty");
jssm.registerASM("RadioButtonFull");

jssm.registerFSM("Pausable", {
    initialState: "running",
    states: {
        paused: "Paused",
        running: "Running"
    }
})

jssm.registerASM("Paused");
jssm.registerASM("Running");

///////////////////////////////////////

let machine = jssm.createMachine("Exam", {
    states: {
        questions: {
            currentState: "q1",
            states: {
                "q1": "Question",
                "q2": "Question",
                "q3": "Question",
            }
        }
    }
})

///////////////////////////////////////

console.log(jssm.debug.registeredMachines);
console.log(jssm.debug.machineInstances);
